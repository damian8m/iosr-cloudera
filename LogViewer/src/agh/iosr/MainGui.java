package agh.iosr;

import java.time.LocalDate;
import java.util.Locale;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker.State;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MainGui extends Application {

	private BorderPane mainPane;
	private HBox critBox;
	private VBox vBox;
	private HBox comBox;
	private DataUtils utils;
	private DatePicker datePicker;
	private TextField sourceIp;
	private TextField destIp;
	private Button search;
	private Button next;
	private Button prev;
	private Label komunikat;
	private TableView table;
	private ServicePobierzLogi service = new ServicePobierzLogi();
	private int beg = 0;
	ProgressIndicator pLogin = new ProgressIndicator();
    Region veilLogin = new Region();
    StackPane stackPane = new StackPane();
    
	public static void main(String[] args) {
		Application.launch(args);
	}

	public void prepareGUI() {
		// przygotuj elementy
		prepareVBox(); // w tym lezy wszystko
		prepareCritBox(); // kryteria wyszukiwania
		prepareMainPane(); // tabela
		prepareComBox(); // komunikaty na dole

	}

	public void prepareVBox() {
		vBox = new VBox();
		vBox.setSpacing(10);
	}

	public void prepareCritBox() {
		// kryteria wyboru na gorze
		critBox = new HBox();
		critBox.setSpacing(10);
		critBox.setStyle("-fx-background-color:#001300");
		critBox.setAlignment(Pos.CENTER);
		critBox.setPadding(new Insets(10, 10, 10, 10));
		datePicker = new DatePicker();
		sourceIp = new TextField();
		destIp = new TextField();

		Label wybierzDateLabel = new Label("Wybierz dat�: ");
		Label sourceIpLabel = new Label("�r�d�owe IP: ");
		Label destIpLabel = new Label("Docelowe IP: ");
		setLabelFont(wybierzDateLabel, "#FEFEFE", 14);
		setLabelFont(sourceIpLabel, "#FEFEFE", 14);
		setLabelFont(destIpLabel, "#FEFEFE", 14);

		critBox.getChildren().add(wybierzDateLabel);
		critBox.getChildren().add(datePicker);
		critBox.getChildren().add(sourceIpLabel);
		critBox.getChildren().add(sourceIp);
		critBox.getChildren().add(destIpLabel);
		critBox.getChildren().add(destIp);
		vBox.getChildren().add(critBox);
	}

	public void setLabelFont(Label label, String color, int size) {
		label.setFont(new Font("Calibri", size));
		label.setTextFill(Color.web(color));
		DropShadow ds = new DropShadow();
		ds.setOffsetY(2.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));
		label.setEffect(ds);
	}

	public void prepareComBox() {
		comBox = new HBox();
		comBox.setSpacing(10);
		comBox.setStyle("-fx-background-color:#D3FE7C");
		comBox.setAlignment(Pos.CENTER);
		comBox.setPadding(new Insets(10, 10, 10, 10));

		komunikat = new Label("Wprowad� kryteria i naci�nij przycisk szukaj");
		komunikat.prefWidthProperty().bind(comBox.widthProperty().divide(2));
		setLabelFont(komunikat, "#000000", 16);

		search = new Button("Szukaj");
		search.prefWidthProperty().bind(comBox.widthProperty().divide(4));
		next = new Button("Nast. strona");
		prev = new Button("Poprz. strona");

		comBox.getChildren().add(komunikat);
		comBox.getChildren().add(search);
		//comBox.getChildren().add(prev);
		//comBox.getChildren().add(next);
		
		search.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent ev) {
				komunikat.setText("�adowanie...");
				service.setBeginning(beg);
				String srcIp = sourceIp.getText();
				String destinationIp = destIp.getText();
				
				if(srcIp != null && !srcIp.equals(""))
					service.setSrcIp(srcIp);
				if(destinationIp != null && !destinationIp.equals("")){
					service.setDestIp(destinationIp);
				}
				
				LocalDate date = datePicker.getValue();
				if(date!= null){
					service.setYear(Integer.toString(date.getYear()));
					service.setMonth(Integer.toString(date.getMonthValue()));
					service.setDay(Integer.toString(date.getDayOfMonth()));
				}
				
				if(service.getState() == State.READY)
					service.start();
				else;
					service.restart();
				
			}
		});
		
		service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent ev) {
				ObservableList<LogEvent> lista = (ObservableList<LogEvent>) ev.getSource().getValue();
				
				if(lista.isEmpty())
					komunikat.setText("Brak rekord�w do wy�wietlenia");
				else
					komunikat.setText("Pobrano rekordy");
				
				table.setItems(lista);
				
			}
		});
		
		service.setOnFailed(new EventHandler<WorkerStateEvent>() {
			
			@Override
			public void handle(WorkerStateEvent ev) {
				ObservableList<LogEvent> lista = (ObservableList<LogEvent>) ev.getSource().getValue();
				
				
					komunikat.setText("B��d po��czenia");
				
				
			}
		});

		mainPane.setBottom(comBox);
		
		veilLogin.setStyle("-fx-background-color: rgba(0, 0, 0, 0.4)");
        pLogin.progressProperty().bind(service.progressProperty());
        veilLogin.visibleProperty().bind(service.runningProperty());
        pLogin.visibleProperty().bind(service.runningProperty());
        pLogin.setMaxSize(150, 150);
        stackPane.getChildren().addAll(vBox, veilLogin, pLogin);
        
	}

	public void prepareMainPane() {
		table = new TableView();
		mainPane = new BorderPane();

		TableColumn srcIpColumn = new TableColumn("IP �r�d�owe");
		srcIpColumn.setPrefWidth(200);
		TableColumn destIpColumn = new TableColumn("IP docelowe");
		destIpColumn.setPrefWidth(200);
		TableColumn srcPortColumn = new TableColumn("Port �r�d�owy");
		TableColumn destPortColumn = new TableColumn("Port docelowy");
		TableColumn dataColumn = new TableColumn("Data");
		dataColumn.setPrefWidth(100);
		TableColumn godzinaColumn = new TableColumn("Godzina");
		
		srcIpColumn.setCellValueFactory(
			    new PropertyValueFactory<LogEvent,String>("srcIp")
			);
		destIpColumn.setCellValueFactory(
			    new PropertyValueFactory<LogEvent,String>("destIp")
			);
		srcPortColumn.setCellValueFactory(
			    new PropertyValueFactory<LogEvent,String>("srcPort")
			);
		destPortColumn.setCellValueFactory(
			    new PropertyValueFactory<LogEvent,String>("destPort")
			);
		dataColumn.setCellValueFactory(
			    new PropertyValueFactory<LogEvent,String>("data")
			);
		godzinaColumn.setCellValueFactory(
			    new PropertyValueFactory<LogEvent,String>("godzina")
			);
		

		table.getColumns().add(srcIpColumn);
		table.getColumns().add(destIpColumn);
		table.getColumns().add(srcPortColumn);
		table.getColumns().add(destPortColumn);
		table.getColumns().add(dataColumn);
		table.getColumns().add(godzinaColumn);
		
		mainPane.setCenter(table);
		mainPane.prefHeightProperty().bind(vBox.heightProperty());
		vBox.getChildren().add(mainPane);
	}

	public void start(Stage stage) throws Exception {
		prepareGUI();
		Locale.setDefault(new Locale("pl_PL"));
		stage.setTitle("Log Viewer");
		stage.setScene(new Scene(stackPane, 800, 600));
		vBox.prefHeightProperty().bind(stage.heightProperty());
		stage.show();

	}

}
