package agh.iosr;

import javafx.beans.property.SimpleStringProperty;

public class LogEvent {

	private SimpleStringProperty srcIp;
	private SimpleStringProperty destIp;
	private SimpleStringProperty srcPort;
	private SimpleStringProperty destPort;
	private SimpleStringProperty data;
	private SimpleStringProperty godzina;
	
	public String getSrcIp() {
		return srcIp.getValue();
	}
	public void setSrcIp(SimpleStringProperty srcIp) {
		this.srcIp = srcIp;
	}
	public String getDestIp() {
		return destIp.getValue();
	}
	public void setDestIp(SimpleStringProperty destIp) {
		this.destIp = destIp;
	}
	public String getSrcPort() {
		return srcPort.getValue();
	}
	public void setSrcPort(SimpleStringProperty srcPort) {
		this.srcPort = srcPort;
	}
	public String getDestPort() {
		return destPort.getValue();
	}
	public void setDestPort(SimpleStringProperty destPort) {
		this.destPort = destPort;
	}
	public String getData() {
		return data.getValue();
	}
	public void setData(SimpleStringProperty data) {
		this.data = data;
	}
	public String getGodzina() {
		return godzina.getValue();
	}
	public void setGodzina(SimpleStringProperty godzina) {
		this.godzina = godzina;
	}
}
