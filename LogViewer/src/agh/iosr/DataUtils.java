package agh.iosr;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataUtils {

	private static String driverName = "org.apache.hive.jdbc.HiveDriver";
	public static Connection con = null;
	
	public static Connection getConnection(){
		try {
		      Class.forName(driverName);
		    } catch (ClassNotFoundException e) {
		      // TODO Auto-generated catch block
		      e.printStackTrace();
		      System.exit(1);
		    }
		if(con == null){
			try {
				con = DriverManager.getConnection("jdbc:hive2://node4.com:10000/default", "hive2", "");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return con;
		}else{
			return con;
		}
		
	}
	
	public static ResultSet executeSQLQuery(String sql){
		Connection conn = getConnection();
		
		
		ResultSet res = null;
		try {
			Statement stmt = conn.prepareStatement(sql);
			res = stmt.executeQuery(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}
	
}
