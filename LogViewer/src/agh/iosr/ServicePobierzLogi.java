package agh.iosr;

import java.sql.ResultSet;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;



public class ServicePobierzLogi extends Service<ObservableList<LogEvent>> {

	private ObservableList<LogEvent> lista;
	private int beg;
	private int end = 1000;
	private String srcIp;
	public ObservableList<LogEvent> getLista() {
		return lista;
	}

	public void setLista(ObservableList<LogEvent> lista) {
		this.lista = lista;
	}

	public int getBeg() {
		return beg;
	}

	public void setBeg(int beg) {
		this.beg = beg;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public String getSrcIp() {
		return srcIp;
	}

	public void setSrcIp(String srcIp) {
		this.srcIp = srcIp;
	}

	public String getDestIp() {
		return destIp;
	}

	public void setDestIp(String destIp) {
		this.destIp = destIp;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		if(month.length()==1)
			this.month = "'0"+month+"'";
		else
			this.month = month;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		if(day.length()==1)
			this.day = "'0"+day+"'";
		else
			this.day = day;
	}

	private String destIp;
	private String year;
	private String month;
	private String day;
	
	public void setBeginning(int beg){
		this.beg = beg;
	}
	
	public void setEnding(int end){
		this.end = end;
	}
	
	protected Task createTask() {
		// TODO Auto-generated method stub
		return new Task<ObservableList<LogEvent>>() {
			protected ObservableList<LogEvent> call() throws Exception {
				int count = 0;
				String query = "select * from networkdata ";
				if(srcIp != null || destIp != null || year!=null){
					query = query + " where ";

					if(srcIp != null){
						if(count > 0)
							query = query + " and ";
						query = query + " src_ip = '"+srcIp+"'";
						count++;
					}
					if(destIp != null){
						if(count > 0)
							query = query + " and ";
						query = query + " dest_ip = '"+destIp+"'";
						count++;
					}
					if(year != null){
						if(count > 0)
							query = query + " and ";
						query = query + " year = "+year;
						query = query + " and month = "+month;
						query = query + " and day = "+day;
						count++;
					}
				}
				
						
				query = query +	" limit " +end;
				System.out.println(query);
				ResultSet res = DataUtils.executeSQLQuery(query);
				
				lista = FXCollections.observableArrayList();
				while(res.next()){
					LogEvent log = new LogEvent();
					String srcIp = res.getString(2);
					String destIp = res.getString(3);
					String srcPort = res.getString(4);
					String destPort = res.getString(5);
					int year = res.getInt(7);
					int month = res.getInt(8);
					int day = res.getInt(9);
					int hour = res.getInt(10);
					
					log.setData(new SimpleStringProperty(year+"/"+month+"/"+day));
					log.setSrcIp(new SimpleStringProperty(srcIp));
					log.setDestIp(new SimpleStringProperty(destIp));
					log.setSrcPort(new SimpleStringProperty(srcPort));
					log.setDestPort(new SimpleStringProperty(destPort));
					log.setGodzina(new SimpleStringProperty(Integer.toString(hour)));
					
					lista.add(log);
				}
				
				System.out.println("Zako�czono pobieranie rekord�w");
				beg = 0;
				srcIp = null;
				destIp = null;
				year = null;
				return lista;
			}
		};

	}

}
